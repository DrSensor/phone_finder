import { Pipe, PipeTransform } from '@angular/core';
import * as _ from "lodash";

/**
 * Generated class for the TurncatePipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'turncate',
})
export class TurncatePipe implements PipeTransform {
  /**
   * Takes a string and turncate that based on width screen.
   */
  transform(value: string, ratio: number) {
    return _.truncate(value, {
      length: window.innerWidth / ratio
    })
  }
}
