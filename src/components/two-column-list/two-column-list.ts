import { Component, Input, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';

/**
 * Generated class for the TwoColumnListComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'two-column-list',
  templateUrl: 'two-column-list.html'
})
export class TwoColumnListComponent implements AfterViewInit, OnChanges {
  @Input('phones') phones: Array<any>

  l_phones: Array<any>
  r_phones: Array<any>

  ngAfterViewInit() {
    this.update()
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.update()
  }

  constructor() {

  }

  update() {
    let numItems = this.phones.length
    this.l_phones = this.phones.slice(0, numItems / 2)
    this.r_phones = this.phones.slice(numItems / 2, numItems)
  }
}
